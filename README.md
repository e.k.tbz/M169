# Willkommen zu meinem Repository für den Modul M169

## Services mit Containern bereitstellen

### Was sind Container?
Container ermöglichen es, Anwendungen mit ihren Abhängigkeiten in einem geschützten Umfeld auszuführen. Alle für die Ausführung einer Anwendung erforderlichen Ressourcen wie Code, Laufzeitumgebung, Systemtools, Bibliotheken und Konfigurationsdateien sind in einem Container enthalten. Die Containerisierungstechnologie wird von Containern verwendet, um sicherzustellen, dass eine Anwendung unabhängig von der zugrunde liegenden Infrastruktur konsistent und vorhersagbar funktioniert.

Container sind im Wesentlichen unabhängige, leichte und transportable Geräte, die es ermöglichen, Anwendungen auf verschiedenen Systemen auszuführen, wie Entwicklungsrechnern, Testumgebungen oder Produktionsinfrastrukturen. Container ermöglichen eine standardisierte Umgebung, in der Anwendungen unabhängig voneinander ausgeführt werden können, ohne sich gegenseitig zu beeinflussen oder Konflikte zu schaffen.

<br>

### Welche 5 Vor- und Nachteile haben Container?

#### Vorteile von Containern:

1. Portabilität: Container sind portierbar und können auf verschiedenen Betriebssystemen und Infrastrukturen laufen, sofern die Container-Laufzeitumgebung verfügbar ist. Dies vereinfacht die Anwendungsbereitstellung und stellt sicher, dass Anwendungen in verschiedenen Umgebungen konsistent ausgeführt werden.
2. Skalierbarkeit: Container ermöglichen eine einfache Skalierung Ihrer Anwendung. Container können isoliert voneinander ausgeführt werden, sodass sie schnell erstellt und geklont werden können, um steigenden Lastanforderungen gerecht zu werden. Dies ermöglicht eine effiziente Ressourcennutzung.
3. Effizienz: Container sind leichtgewichtig und teilen sich den Kernel des Hostsystems, sodass sie im Vergleich zu virtuellen Maschinen weniger Systemressourcen beanspruchen. Dadurch können mehrere Container auf derselben Infrastruktur ausgeführt werden, wodurch die Ressourcen effizienter genutzt werden. 4. Isolation: Container bieten eine isolierte Umgebung für Anwendungen und reduzieren so Konflikte und Abhängigkeitsprobleme. Jeder Container verfügt über eine eigene Laufzeitumgebung, die von anderen Containern und dem Hostsystem isoliert ist, was zu besserer Sicherheit und Stabilität führt.
5. Schnelle Bereitstellung und DevOps-Unterstützung: Container ermöglichen eine schnelle Anwendungsbereitstellung durch die Bereitstellung vorkonfigurierter Container. Dies erleichtert auch die Integration in Ihre CI/CD-Pipeline und fördert DevOps-Prinzipien, indem es Entwicklern und Betriebsteams eine einfache Zusammenarbeit ermöglicht.

#### Nachteile von Containern:

1. Komplexität der Orchestrierung: Die Orchestrierung und Verwaltung von Containern im großen Maßstab kann komplex sein. Die Verwendung von Orchestrierungstools wie Kubernetes erfordert zusätzliche Kenntnisse und kann eine steile Lernkurve erfordern.
2. Sicherheitsbedenken: Obwohl Container eine isolierte Umgebung bieten, können Schwachstellen in Containern oder Hostsystemen zu potenziellen Risiken führen. Um die Sicherheit Ihrer Container zu gewährleisten, sind eine ordnungsgemäße Sicherheitskonfiguration und regelmäßige Updates erforderlich.
3. Persistente Datenverwaltung: Container sind in der Regel kurzlebig konzipiert. Dies bedeutet, dass Daten im Container verloren gehen können, wenn der Container gestoppt oder gelöscht wird. Die Verwaltung persistenter Daten erfordert zusätzliche Konfiguration oder den Einsatz externer Speicherlösungen. 
4. Ressourcenbeschränkungen: Container nutzen Ressourcen effizient, teilen sich jedoch den Kernel des Hostsystems. Dies bedeutet, dass bestimmte Ressourcen wie CPU und Speicher von Containern gemeinsam genutzt werden und eine übermäßige Nutzung durch einen Container die Leistung anderer Container beeinträchtigen kann.
5. Eingeschränkte GUI-Unterstützung: Container sind in erster Linie darauf ausgelegt, Anwendungen im Hintergrund auszuführen.

<br>

[Hier geht es zu der Projektdokumentation:](Dokumentation/Dokumentation M169.md)
<br>
[Hier geht es zum Lernjournal](Lernjournal/Lernjournal.md)
