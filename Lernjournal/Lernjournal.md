# Tag 1: 24.02.2023

## Um eine vollständig funktionsfähige Toolumgebung einzurichten, habe ich die folgenden Schritte durchgeführt:

### 1\. Gitlab-Account einrichten:

- Ein Benutzerkonto auf www.gitlab.com erstellt, mit Angabe von Username, E-Mail und Passwort.

- Die E-Mail zur Verifizierung des Kontos bestätigt und mich anschließend bei Gitlab angemeldet.

- Ein Repository erstellt, indem ich mich angemeldet und auf der Willkommensseite auf "Start a project" geklickt habe. Dabei habe ich einen Namen und eine optionale Beschreibung festgelegt, das Repository als öffentlich markiert und mit einem README initialisiert.

- Einen SSH-Key lokal erstellt, indem ich den Befehl "ssh-keygen -t rsa -b 4096 -C 'beispiel@beispiel.com'" in der Git/Bash ausgeführt habe. Das Passwort für den Key festgelegt und dem SSH-Agent hinzugefügt.

- Den SSH-Key meinem Gitlab-Account hinzugefügt, indem ich mich auf www.gitlab.com angemeldet, auf mein Benutzerkonto geklickt und den Abschnitt "SSH und GPG keys" aufgerufen habe. Dort habe ich einen neuen SSH-Key mit Titel und dem zuvor kopierten Key erstellt.

### 2\. Git-Client installieren und konfigurieren:

- Den Git-Client (Git/Bash) von der offiziellen Webseite heruntergeladen und installiert.

- Die Git-Konfiguration mit den Informationen des Gitlab-Accounts durchgeführt, indem ich in der Git/Bash die Befehle "git config --global user.name '<username>'" und "git config --global user.email '<e-mail>'" ausgeführt habe.

- Ein Repository geklont, indem ich in der Git/Bash den Befehl "git clone https://gitlab.com/ch-tbz-it/Stud/m169" ausgeführt habe. Anschließend bin ich in das Verzeichnis gewechselt, das Repository aktualisiert und den Status überprüft.

### 3\. VirtualBox und Vagrant installieren:

- Ich wollte VirtualBox herunterladen, dabei musste ich für meinen Mac die App Xcode downloaden. Leider war die App so gross, dass ich nicht mehr weiter machen konnte.

## Ziele für nächste Woche:
Die Toolumgebung fertig zu haben, damit ich dann weiter arbeiten kann.

<br>

# Tag 2: 03.03.2023

### 3\. VirtualBox und Vagrant installieren:

- Ich wollte das letzte mal VirtualBox herunterladen, dabei musste ich feststellen, dass ich noch eine App benötigte, die ser gross war. Deswegen hatte ich heute den Auftrag mit Vagrant heute erledigt.

- Vagrant und VirtualBox über Terminal instlliert

- Eine virtuelle Maschine erstellt, indem ich in der Git/Bash einen neuen Ordner angelegt und das Vagrantfile erzeugt habe. Anschließend habe ich die virtuelle Maschine mit dem Befehl "vagrant up --provider virtualbox" gestartet.

- Die virtuelle Maschine über SSH-Zugriff bedient, indem ich in der Git/Bash in das Verzeichnis der VM gewechselt und den Befehl "vagrant ssh" ausgeführt habe. Dort konnte ich normale Bash-Befehle wie "ls", "df" und "free" ausführen.

- Die virtuelle Maschine über die VirtualBox-GUI ausgeschaltet.

#### 4\. Automatisierte Einrichtung eines Apache-Webservers:

- Eine VM so eingerichtet, dass sie mit einem vorinstallierten Apache-Webserver startet.

##### Durch diese Schritte wurde eine vollständig funktionsfähige Toolumgebung eingerichtet, die Gitlab, VirtualBox, Vagrant und Visual Studio Code umfasst.

## Cloud Computing-Dienstleistungsmodelle - IaaS, PaaS, SaaS erklärt

Cloud Computing bezieht sich auf die Ausführung von Programmen auf entfernten Computern, anstatt sie lokal auf dem eigenen Computer zu installieren. Es ermöglicht die Bereitstellung von IT-Infrastruktur (wie Rechenleistung, Datenspeicher, Datensicherheit, Netzwerkkapazität oder fertige Software) über ein Netzwerk, ohne dass eine lokale Installation erforderlich ist. Dies geschieht über technische Schnittstellen, Protokolle und Browser. Das Cloud Computing umfasst verschiedene Dienstleistungsmodelle wie Infrastruktur-as-a-Service (IaaS), Plattform-as-a-Service (PaaS) und Software-as-a-Service (SaaS).

## Arten von Cloud Computing

### Infrastruktur-as-a-Service (IaaS):
IaaS stellt die grundlegende Infrastruktur bereit und ermöglicht den Nutzern den Zugriff auf Recheninstanzen (virtuelle Maschinen). Im Gegensatz zu PaaS haben die Nutzer bei IaaS direkte Kontrolle über die Recheninstanzen und betreiben virtuelle Server.

## Plattform-as-a-Service (PaaS):
PaaS ermöglicht Entwicklern die Erstellung und Bereitstellung von Anwendungen in der Cloud. Die Cloud übernimmt die Verteilung der Anwendungen auf die zugrunde liegenden Ressourcen. Im Gegensatz zu IaaS haben die Nutzer keinen direkten Zugriff auf die Recheninstanzen und betreiben keine virtuellen Server.

## oftware-as-a-Service (SaaS):
SaaS stellt Anwendungen über die Cloud bereit. Nutzer müssen weder ihre eigene Anwendung entwickeln noch sich um Skalierbarkeit oder Datenspeicherung kümmern. Stattdessen nutzen sie eine vorhandene Anwendung, die vom Cloud-Anbieter bereitgestellt wird.

Zusätzlich zu diesen Modellen gibt es eine weitere Schicht, die mit dem Aufkommen von Docker (Containerisierung) entstanden ist:

## Container-as-a-Service (CaaS):
CaaS ermöglicht die Ausführung von containerisierten Arbeitslasten auf den von einer IaaS-Cloud bereitgestellten Ressourcen. Technologien wie Docker, Kubernetes oder Mesos gehören zu dieser Schicht und bieten eine flexible und portable Lösung für die Bereitstellung von Anwendungen.

## Dynamische Infrastrukturplattformen

Beispiele für dynamische Infrastrukturplattformen sind Microsoft Azure, AWS, Google Cloud und andere. Diese Plattformen virtualisieren und stellen Rechenressourcen wie CPU, Speicher und Netzwerke bereit. Die Zuweisung und Verwaltung dieser Ressourcen erfolgt programmatisch und die Bereitstellung erfolgt über virtuelle Maschinen (VMs). Es gibt sowohl öffentliche Cloud-Anbieter als auch private Cloud-Plattformen wie CloudStack, OpenStack und VMware vCloud. Hyperkonvergente Systeme bieten eine integrierte Lösung, die verschiedene Funktionen in einer einzigen Hardware vereint.

<br>

# Tag 3: 10.03.2023

## 01 - Firewall & Reverse Proxy

Bis jetzt sind alle Services ungehindert zugreifbar. Wenn Sie eine VM direkt in das Internet oder in eine DMZ stellen würden, hätten Sie ein größeres Sicherheitsproblem. Um dies zu verhindern, sollten Sie nicht-öffentliche Ports mittels einer Firewall sperren und den restlichen Datenverkehr mit einem Reverse Proxy verschlüsseln.

### Firewall

Eine Firewall ist ein Sicherungssystem, das ein Rechnernetz oder einen einzelnen Computer vor unerwünschten Netzwerkzugriffen schützt und ein Teilaspekt eines Sicherheitskonzepts darstellt.

Jede Firewall basiert auf einer Softwarekomponente. Die Firewall-Software dient dazu, den Netzwerkzugriff basierend auf Absender- oder Zieladresse und genutzten Diensten zu beschränken. Sie überwacht den Datenverkehr, der durch die Firewall läuft, und entscheidet anhand festgelegter Regeln, ob bestimmte Netzwerkpakete durchgelassen werden oder nicht. Auf diese Weise versucht sie, unerlaubte Netzwerkzugriffe zu verhindern.

### Reverse Proxy

Ein Reverse Proxy ist ein Proxy, der Ressourcen für einen Client von einem oder mehreren Servern holt. Die Adressumsetzung wird dabei in der entgegengesetzten Richtung vorgenommen, wodurch die wahre Adresse des Zielsystems dem Client verborgen bleibt. Während ein typischer Proxy dafür verwendet werden kann, mehreren Clients eines internen (privaten) Netzes den Zugriff auf ein externes Netz zu ermöglichen, funktioniert ein Reverse Proxy genau andersherum.

### UFW Firewall

UFW steht für Uncomplicated Firewall. Das Ziel von UFW ist es, ein unkompliziertes, kommandozeilenbasiertes Frontend für das leistungsstarke, aber nicht gerade einfach zu konfigurierende iptables zu bieten. UFW unterstützt sowohl IPv4 als auch IPv6.

#### Ausgabe der offenen Ports:

$ netstat -tulpen

### Installation:

$ sudo apt-get install ufw

### Start / Stop:

$ sudo ufw status

$ sudo ufw enable

$ sudo ufw disable

### Firewall-Regeln:

# Port 80 (HTTP) für alle öffnen

vagrant ssh web

sudo ufw allow 80/tcp

exit

# Port 22 (SSH) nur für den Host (wo die VM läuft) öffnen

vagrant ssh web
w
sudo ufw allow from [Meine-IP] to any port 22
exit

# Port 3306 (MySQL) nur für den Web-Server öffnen

vagrant ssh database

sudo ufw allow from [IP der Web-VM] to any port 3306

exit

### Zugriff testen:

$ curl -f 192.168.55.101

$ curl -f 192.168.55.100:3306

### Löschen von Regeln:

$ sudo ufw status numbered

$ sudo ufw delete 1

### Ausgehende Verbindungen:

Ausgehende Verbindungen werden standardmäßig erlaubt. Wenn Sie keine ausgehenden Verbindungen benötigen oder nur bestimmte (z.B. SSH), können Sie zuerst alle Verbindungen schließen und dann einzelne Verbindungen freischalten.

$ sudo ufw deny out to

### Ziel für nächste Woche: 
Mit nächsten Kapitel anfangen. (30-Container)

<br>

# Tag 4: 17.03.2023
### Microservices:
Docker-Microservices sind eine moderne Möglichkeit, Ihre Anwendung in kleine, unabhängige und leichte Komponenten aufzuteilen, die als Container ausgeführt werden. Jeder Microservice erfüllt eine bestimmte Funktion und kann unabhängig entwickelt, bereitgestellt und skaliert werden. Docker-Microservices nutzen Container, um eine flexible, skalierbare und effiziente Architektur für die Anwendungsentwicklung und -bereitstellung zu ermöglichen. Sie fördern einen modularen, verteilten Ansatz, der eine verbesserte Wartbarkeit, Skalierbarkeit und Kontinuität von Anwendungen ermöglicht.

### Docker:
Ich habe mir das Video zur Einführung von Docker angeschaut und Docker installiert.

### Docker Architektur
Die Docker-Architektur besteht aus drei Hauptkomponenten: Docker-Client, Docker-Daemon und Containern. Der Docker-Client ist eine Benutzeroberfläche, die Befehle ausführt, die mit dem Docker-System interagieren. Der Docker-Daemon ist ein Hintergrundprozess, der für die Erstellung, Ausführung und Verwaltung von Containern verantwortlich ist. Ein Container ist eine isolierte Umgebung, in der eine Anwendung und ihre Abhängigkeiten ausgeführt werden können. Sie nutzen die Ressourcen des Host-Betriebssystems und sind portabel, leichtgewichtig und skalierbar. Die Docker-Architektur ermöglicht Entwicklern und Administratoren das effiziente Paketieren, Bereitstellen und Skalieren von Anwendungen unabhängig von der zugrunde liegenden Infrastruktur.

### Ziel für nächste Woche:
30-Container abschliessen.

<br>

# Tag 5: 24.03.2023

### Was habe ich heute gemacht?
Ich habe den kapitel 30-Container abgeschlossen und mit die verschiedenen Docker Befehle ausprobiert. Dabei hatte ich Schwierigkeiten, weil ich nicht alles verstanden hatte und viel recherchieren musste.

### Die wichtigsten Docker Befehle:
1. „Docker Run“: Starten Sie einen neuen Container aus einem Image. 
2. „Docker Build“: Erstellen Sie ein Docker-Image aus einer Docker-Datei. 
3. „Docker Pull“: Laden Sie ein Docker-Image aus einem Repository herunter. 
4. „docker ps“: Zeigt eine Liste der ausgeführten Container an. 
5. „Docker-Image“: Zeigt eine Liste der verfügbaren Docker-Images an. 
6. „Docker Stop“: Stoppen Sie einen laufenden Container. 
7. „docker rm“: Entfernen Sie den gestoppten Container. 
8. Platz. „docker rmi“: Entfernen Sie ein Docker-Image. 
9. „docker exec“: Führen Sie einen Befehl in einem laufenden Container aus. 
10. „Docker-Protokolle“: Containerprotokolle anzeigen. 11. „Docker-Netzwerk“: Docker-Netzwerke verwalten. 
12. „docker-compose up“: Starten Sie einen Container basierend auf einer Docker Compose-Datei. 
13. „docker-compose down“: Stoppen und entfernen Sie Container, Netzwerke und Volumes, die mit Docker Compose gestartet wurden. 
14. „Docker-Inspektion“: Detaillierte Informationen zu einem Container oder Image anzeigen. 
15. „docker-compose build“: Erstellen Sie ein Docker-Image basierend auf einer Docker Compose-Datei. Diese Befehle bilden eine hervorragende Grundlage für die Arbeit mit Docker und das Erstellen, Verwalten und Arbeiten mit Containern. Abhängig von Ihren spezifischen Anforderungen und Szenarien können weitere Befehle und Optionen untersucht werden.

### Ziel für nächste Woche:
Möglichst Kapitel 35-Sicherheit abschliessen.

<br>

# Tag 6: 31.03.2023

### Was habe ich heute gemacht?
Ich habe am Kaptiel 35-Sicherheit gearbeitet und abgeschlossen. Ich habe mich im Internet noch um weitere Sicherheits tips zu Container recherchiert.

### best Practises für Sciherheit in Container:
1. Verwenden Sie offizielle und vertrauenswürdige Images: Verwenden Sie Images aus vertrauenswürdigen Quellen wie Docker Hub und offiziellen Image-Repositories. Überprüfen Sie die Beliebtheit, Aktualität und Bewertungen von Bildern.
2. Regelmäßig aktualisieren: Halten Sie Ihre Images und Container mit regelmäßigen Updates und Sicherheitspatches auf dem neuesten Stand. Beachten Sie außerdem die Sicherheitshinweise des verwendeten Images.
3. Minimieren Sie Ihre Angriffsfläche: Reduzieren Sie Ihre Angriffsfläche, indem Sie unnötige Tools und Dienste aus Ihren Containern entfernen. Beschränken Sie die angebotenen Ports und öffnen Sie nur die Ports, die für den Zugriff auf Ihre Anwendung erforderlich sind.
Vier. Container-Isolierung: Verwenden Sie verschiedene Netzwerke und Benutzergruppen, um Container voneinander zu isolieren. Vermeiden Sie die Verwendung privilegierter Container, es sei denn, dies ist unbedingt erforderlich.
5. Ressourcenlimits festlegen: Legen Sie CPU-, Speicher- und Netzwerkressourcenlimits fest, um den Missbrauch von Ressourcen durch Container zu verhindern und die Leistung des Hostsystems zu schützen.
6. Containerüberwachung: Implementieren Sie Überwachungsmechanismen, um Container auf verdächtige Aktivitäten zu überwachen. Nutzen Sie Protokollierung und Überwachung, um potenzielle Sicherheitsvorfälle frühzeitig zu erkennen.
7. Authentifizierung und Zugriffskontrolle: Verwenden Sie sichere Passwörter für den Container- und Anwendungszugriff. Implementieren Sie Zugriffskontrollen, authentifizieren Sie Benutzer und verhindern Sie unbefugten Zugriff. 8. Volume-Verschlüsselung: Wenn Sie sensible Daten in Containern speichern, verschlüsseln Sie die Volumes, um den Schutz im Ruhezustand zu gewährleisten.
9. Vertrauenswürdige Bildquellen: Verwenden Sie nur vertrauenswürdige Bildquellen und überprüfen Sie Hashes, um die Bildintegrität zu überprüfen.
Zehn Kontinuierliches Sicherheitsbewusstsein: Bleiben Sie über die neuesten Sicherheitslücken und Best Practices auf dem Laufenden. Steigern Sie das Sicherheitsbewusstsein im Team und fördern Sie eine Sicherheitskultur.
Diese Best Practices sind ein guter Ausgangspunkt für die Verbesserung der Containersicherheit. Denken Sie daran, dass Sicherheit ein fortlaufender Prozess ist und regelmäßige Überprüfungen und Aktualisierungen erfordert, um mit der sich ständig ändernden Bedrohungslandschaft Schritt zu halten.

### Ziel für nächste Woche?
Mit meinen eigenen Docker Projekt anzufangen.

<br>

# Tag 7: 14.04.2023
### Was hab ich heute gemacht:
Ich habe Ideen gesammelt und im Internet recherchiert, was man machen könnte.
Nachdem habe ich mich mal mit nginx ausseinander gesetzt und es installiert.
Dabei hatte ich einige Probleme, vorallem als ich ein Dockerfile erstellen wollte. Ich wusste nicht wo ich diesen dockerfile erstellen musste und auch nicht was man alles mit dem dockerfile machen konnte für nginx.
Das hat mir sehr viel Zeit gekostet.

### Ziel für nächste Woche?
Eindeutig zu wissen, was ich genau machen will und nginx abschliessen.

<br>

# Tag 8: 21.04.2023
### Was hab ich heute gemacht:
Ich habe nginx nach einer längeren Zeit abgeschlossen und habe weiter nachgeschaut, was ich zusätzlich machen könnte.
Da ich mich nicht entscheiden konnte habe ich den heutigen Tag "verschwendet", da es einige coole Idee gab.
Am Schluss hatte ich mich einfach auf eine eigene Website und myPhpAdmin und MariaDB festgelegt, damit ich weiss was ich in den Ferien machen muss.

### Ziel für die nächste Woche:
Mein Projekt so weit wie möglich abzuschlissen, damit ich am Freitag nur noch die Dokumentation und Lernjournal machen muss.

<br>

# Tag 9: 12.05.2023
### Was hab ich in den Ferien gemacht:
Ich hatte in den Ferien eine Idee und zwar mit nginx, phpMyAdmin und MariaDB eine Website zu erstellen, wo man sich regristieren kann. Die Daten von der Regristierung sollten, dann in die Datenbank aufgenommen werden und nach der Regristierung sich einzuloggen. Leider war ich in den Ferien sehr beschäftigt und hatte nicht die Möglichkeit weiter zu areiten und dazu kommt dass meine Idee recht viel Zeit beansprucht hätte.

### Was habe ich heute gemacht?
ich habe heute vorwiegend die Dokumentation erstellt und alles auf Gitlab hochgelden.
