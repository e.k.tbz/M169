# Dokumentation M169

## Einleitung:

Docker ist eine Open-Source-Plattform, die es Ihnen ermöglicht, Anwendungen in isolierten Containern auszuführen. Mit Docker können Entwickler Softwareumgebungen erstellen und Anwendungen und ihre Abhängigkeiten einfach bereitstellen. Die Verwendung von Containern vereinfacht die Anwendungswartung, da Container portabel und skalierbar sind. Docker ermöglicht eine effiziente Nutzung von Ressourcen und fördert die Kontinuität bei der Entwicklung, Bereitstellung und Skalierung von Anwendungen.

## Docker installieren:
Von Docker selbst wurde eine Installationsdatei bereitgestellt für MacBooks. Dadurch musste ich nur Docker Desktop installieren und hatte Docker schon fertig installiert.

## nginx istallieren via dockerfile:

Um nginx zu installieren habe ich zuerst den nginx:alpine Image gepullt.
Code: docker pull nginx:alpine

Danach habe ich einen Ordner erstell, wo ich den dockerfile und den Index.html erstellt habe. 

Das dockerfile sieht wie folgt aus: 
<br>
![dockerfile](Aspose.Words.94357ff5-6d41-443e-a141-933276bd68f5.001.jpeg)

### Was macht das dockerfile im Bild?

Zunächst wird die Basis des Docker-Images als nginx:latest definiert, wobei die neueste Version des offiziellen Nginx-Images als Basis verwendet wird.

Zweitens kopiert es alle HTML-Dateien aus dem aktuellen Verzeichnis (in dem sich die Docker-Datei befindet) in das Verzeichnis /usr/share/nginx/html/ im Container. Dadurch werden die HTML-Dateien in das Nginx-Dokumentverzeichnis im Container kopiert, sodass sie beim Start des Containers vom Webserver aus zugänglich sind.

Den Index.html habe ich selber erstellt und das sieht so aus: 
<br>
![Index.html](Aspose.Words.94357ff5-6d41-443e-a141-933276bd68f5.002.jpeg)


Nachdem ich den dockerfile und Index.html fertig hatte habe ich folgendes ausgeführt: ![Terminal zu dockerfile](Aspose.Words.94357ff5-6d41-443e-a141-933276bd68f5.003.jpeg)

Ich habe dann geschaut, ob nginx lauft. Dazu habe ich einmal localhost:80 auf dem Browser geöffnet und 0.0.0.0:80. ![nginx Website](Aspose.Words.94357ff5-6d41-443e-a141-933276bd68f5.004.jpeg)


## phpMyAdmin und MariaDB installieren via docker-compose:

Für phpMyAdmin und MariaDB habe ich es mit docker-compose gestartet. Dafür habe ich einen neuen Ordner erstell und dort den docker-compose.yml erstellt

Im docker-compose.yml steht folgendes: 
<br>
![docker-compose.yml](Aspose.Words.94357ff5-6d41-443e-a141-933276bd68f5.005.jpeg)


### Was macht das docker-compose.yml?

Diese Docker Compose-Datei definiert zwei Dienste: "mariadb" und "phpmyadmin".

Der Dienst "mariadb" verwendet das Docker-Image "mariadb:latest" und erstellt einen Container mit dem Namen "mariadb". Legt Umgebungsvariablen wie "PUID" und "PGID" für Benutzer- und Gruppenberechtigungen, "MYSQL\_ROOT\_PASSWORD" für das MariaDB-Root-Passwort und "TBZ" für die Zeitzone des Containers fest. Zur Speicherung der Datenbankdateien wird das Volume „/Users/Emir/mariadb/“ verwendet. Der Dienst wird automatisch neu gestartet, sofern er nicht explizit gestoppt wird.

Der phpmyadmin-Dienst verwendet das phpmyadmin-Docker-Image und hostet den Dienst auf Host-Port 8080. Der Dienst wird immer automatisch neu gestartet. Die Umgebungsvariable "PMA\_ARBITRARY" ist auf 1 gesetzt, was bedeutet, dass Sie eine Verbindung zu jedem MySQL-Server herstellen können.

Im Allgemeinen stellt diese Docker Compose-Datei die MariaDB-Datenbank und die PHPMyAdmin-Schnittstelle bereit, die auf Host-Port 8080 verfügbar sind. Datenbankdateien werden auf einem bestimmten Volume gespeichert.


Nachdem ich das docker-compose.yml fertig hatte, habe ich "docker-compose up -d" ausgeführt.
Das hat so ausgesehen: ![Terminal zu docker-compose](Aspose.Words.94357ff5-6d41-443e-a141-933276bd68f5.006.jpeg)



Nachdem bin ich auf dem Browser auf diese IP "0.0.0.0:8080" gegangen. Da habe ich bei Server mariadb, root und TBZ eingefügt. 
<br>
![phpMyAdmin Bild 1](Aspose.Words.94357ff5-6d41-443e-a141-933276bd68f5.007.png) ![phpMyAdmin Bild 2](Aspose.Words.94357ff5-6d41-443e-a141-933276bd68f5.008.jpeg)



Docker Desktop sieht wie folgt aus: 
<br>
![Docker Desktop Bild 1](Aspose.Words.94357ff5-6d41-443e-a141-933276bd68f5.009.jpeg) ![Docker Desktop Bild 2](Aspose.Words.94357ff5-6d41-443e-a141-933276bd68f5.010.jpeg)

## Fazit:
Am Anfang habe ich Docker unterschätzt und dachte, dass es einfach wäre. Jedoch habe ich schnell gemerkt, dass es mehr Komplexität birgt als gedacht. Durch verschiedene YouTube-Videos und Websites habe ich im Laufe der Zeit viel gelernt. Besonders habe ich gelernt, worauf man achten sollte, und wenn ich auf Probleme gestoßen bin, gab es zahlreiche Websites, die mir dabei geholfen haben.

Insgesamt hat es mir sowohl Spaß als auch Frustration bereitet, aber am Ende war ich sehr zufrieden, dass alles funktioniert hat.
